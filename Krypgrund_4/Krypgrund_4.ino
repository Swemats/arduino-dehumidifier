/* Empty waterbucket by controling a submergable pump using a level sensor
 *
 * Hardware and code based on 
 * http://lifeboatfarm.wordpress.com/2009/12/28/arduino-water-level-gauge/
 * 
 * and http://pompie-arduino.blogspot.se/2007_01_01_archive.html
 *
 *
 * Part of:
 * 
 * Controlling a dehumidifier in suspended foundation
 *
 * Step 1
 * Develop method to log humidity and temperature using DHT 11: Krypgrund_3.ino
 *
 * Step 2
 * Develop method to controlling pump for emty water bucket using a water 
 * level sensor: Krypgrund_4.ino
 *
 */

int StartLevel = 540;
int MaxLevel = 550;
int StopLevel = 513;
int levelSensor;
long levelCount;		// summed values for avarage
int levelAverage;		// avarege value of level

void setup()
{
	Serial.begin(9600);
	Serial.println("Water level test!");

	/* 1 output and set to default 5V (OFF/LOW)
	 * Relay opens on LOW	
	 */
	pinMode(10, OUTPUT);		// Signal to relay
	digitalWrite(10, HIGH);
}

void loop()
{	
	// Read value from Analog input 0
	//levelSensor = analogRead(0);

	
	levelCount = 0;
	
	// take 20 readings
	for (int i=0; i < 20; i++)
	{
		levelSensor = analogRead(0);
		levelCount += levelSensor;
		delay(10);
	}
  
	// calculate the average
	levelAverage = levelCount / 20;
	
	// levelAverage = levelSensor;

	
	if(levelAverage > MaxLevel)
	{
		Serial.println("Warning, to much water!");
		digitalWrite(10, HIGH);
	}
	else if(levelAverage < MaxLevel && levelAverage > StartLevel)
	{
		Serial.println("Start pump");
		digitalWrite(10, LOW);
	}
	else if(levelAverage < StopLevel)
	{
		Serial.println("Stop pump");
		digitalWrite(10, HIGH);
	}
	

	// Print raw value on UART (USB)
	Serial.print("A0=");
	Serial.println(levelAverage);

	// Wait a bit so characters have a chance to be transmitted.
	delay(1000);
}
