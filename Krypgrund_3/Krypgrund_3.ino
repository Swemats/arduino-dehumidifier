/* Log humidity and temperature
 * Send log-data to server using serial communication
 *
 * Part of code from Adafruit Learning http://learn.adafruit.com/dht/overview 
 * and Lawicel Blog http://www.lawicel.se/blog/?page_id=359
 *
 *
 * Part of:
 * 
 * Controlling a dehumidifier in suspended foundation
 *
 * Step 1
 * Develop method to log humidity and temperature using DHT 11: Krypgrund_3.ino
 * 
 */

#include "DHT.h"
#define DHTPIN 2			// pin DHT is connected to

#define DHTTYPE DHT11		// DHT 11

DHT dht(DHTPIN, DHTTYPE);

void setup()
{
    Serial.begin(9600);
    Serial.println("DHTxx test!");
    dht.begin();

    // 2 outputs and set them to default 0V (OFF/LOW)
    pinMode(10, OUTPUT);		// Signal to motor
    pinMode(11, OUTPUT);		// Signal to motor
    pinMode(13, OUTPUT);		// Signal to relay
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
}

void loop()
{
    // Start motor
    analogWrite(10, 65);
    
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

   float hum = dht.readHumidity();
   float temp = dht.readTemperature();
  
   // check if returns are valid, if they are NaN (not a number) 
   // then something went wrong!
   if(isnan(temp) || isnan(hum))
     Serial.println("Failed to read from DHT");
   else
   {
     Serial.print("Humidity: ");
     Serial.print(hum);
     Serial.print(" %\t");
     Serial.print("Temperature: ");
     Serial.print(temp);
     Serial.println(" *C");
   }

   if(temp > 26.0)
       digitalWrite(13, LOW);	// Relay open - motor stops
   else
       digitalWrite(13, HIGH);	// Relay close - motor running
}
