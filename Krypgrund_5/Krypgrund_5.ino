/* Date and time functions using a DS1307 based RTC (Tiny RTC) connected via I2C and Wire lib
 *
 * Code based on Adafruit Learning
 * http://learn.adafruit.com/ds1307-real-time-clock-breakout-board-kit/overview 
 * connecting RTC directly to headers on Arduino board
 * 
 * Part of:
 * 
 * Controlling a dehumidifier in suspended foundation
 *
 * Step 1
 * Develop method to log humidity and temperature using DHT 11: Krypgrund_3.ino
 *
 * Step 2
 * Develop method to controlling pump for emty water bucket using a water
 * level sensor: Krypgrund_4.ino
 *
 * Step 3
 * Develop metod to use RTC to get time tag: Krypgrund_5.ino
 *
 */
 
#include <Wire.h>
#include "RTClib.h"
 
RTC_DS1307 RTC;
 
void setup () {
  // Place RTC directly on female header 2-5
  pinMode(16, OUTPUT);		// Analog pin 2, GND on RTC
  pinMode(17, OUTPUT);		// Analog pin 3, VCC on RTC
  digitalWrite(16, LOW);
  digitalWrite(17, HIGH);
  
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
     
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // RTC.adjust(DateTime(__DATE__, __TIME__));
  }
 
}
 
void loop () {
    DateTime now = RTC.now();
 
    String year = String(now.year(), DEC);
    String month = String(now.month(),DEC);
    String day = String(now.day(), DEC);
    String hour = String(now.hour(), DEC);
    String minute = String(now.minute(), DEC);
    String second = String(now.second(), DEC);
    
    if(month.length() < 2)
      month = "0" + month;
        
    if(day.length() < 2)
      day = "0" + day;
      
    if(hour.length() < 2)
      hour = "0" + hour;
      
    if(minute.length() < 2)
      minute = "0" + minute;
    
    if(second.length() < 2)
      second = "0" + second;
    
    String date = year + "-" + month + "-" + day;
    String time = hour + ":" + minute + ":" + second;
    
    Serial.println(date + " " + time);
 
    delay(3000);
}
