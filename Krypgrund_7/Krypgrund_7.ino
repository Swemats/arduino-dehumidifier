/* Test trying to send logged data with a UART WiFi modul (TLG10UA03)
 *
 * Humidity and temperature are logged with time stamp using a DHT22 based sensor
 * Date and time functions using a DS1307 based RTC (Tiny RTC) connected via I2C and Wire lib
 * Send log-data to server using serial communication
 *
 * Part of code from Adafruit Learning http://learn.adafruit.com/dht/overview and
 * http://learn.adafruit.com/ds1307-real-time-clock-breakout-board-kit/overview 
 * and Arduino Cookbook 2nd Ed.
 *
 * Part of:
 * 
 * Controlling a dehumidifier in suspended foundation
 *
 * Step 1
 * Develop method to log humidity and temperature using DHT 11: Krypgrund_3.ino
 *
 * Step 2
 * Develop method to controlling pump for emty water bucket using a water
 * level sensor: Krypgrund_4.ino
 *
 * Step 3
 * Develop metod to use RTC to get time tag: Krypgrund_5.ino
 * 
 * Step 4
 * Combined Step 1 and 3 to generat an csv-output
 * Output format: Date Time, Humidity, Temperatur
 * Date format: YYYY-MM-DD
 * Time format: HH:MM:SS
 *
 * Step 5
 * Using UART WiFi modul to send the data wireless
 * Data is sent when on a HTTP request/call on the modules "FTP-server"
 *
 */

#include <Wire.h>
#include "RTClib.h"
#include "DHT.h"

#define DHTPIN 2		// pin DHT is connected to
#define DHTTYPE DHT22		// DHT 22
 
RTC_DS1307 RTC;
DHT dht(DHTPIN, DHTTYPE);

void setup()
{
	// Place RTC directly on female header 2-5
  pinMode(16, OUTPUT);		// Analog pin 2, GND on RTC
  pinMode(17, OUTPUT);		// Analog pin 3, VCC on RTC
  digitalWrite(16, LOW);
  digitalWrite(17, HIGH);
  
  Serial.begin(9600);
  dht.begin();
  Wire.begin();
  RTC.begin();
   
   if (! RTC.isrunning())
   {
	 Serial.println("RTC is NOT running!");
	 // following line sets the RTC to the date & time this sketch was compiled
	 // RTC.adjust(DateTime(__DATE__, __TIME__));
   }
}
void loop()
{
	boolean currentLineIsBlank = true;
	while(1)
	{
		if (Serial.available())
		{
			char c = Serial.read();

			/* if we gotten to the end of the line (received a newline
			 * character) and the line is blank, the http request has ended,
			 * so we can send a reply
			 */
			if (c == '\n' && currentLineIsBlank)
			{
				// send a standard http response header
				Serial.println("HTTP/1.1 200 OK");
				Serial.println("Content-Type: text/html");
				Serial.println();
				
				// send the time tagged data
				printLogData();
				break;
			}
			if (c == '\n')
			{
				// we're starting a new line
				currentLineIsBlank = true;
			}
			else if (c != '\r')
			{
				// we've gotten a character on the current line
				currentLineIsBlank = false;
			}
		}
	}
}


void printLogData()
{
   float hum = dht.readHumidity();
   float temp = dht.readTemperature();
  
   // check if returns are valid, if they are NaN (not a number) 
   // then something went wrong!
   if(isnan(temp) || isnan(hum))
	 Serial.println("Failed to read from DHT");
   else
   {
	 Serial.print(timestamp() + ",");
	 Serial.print(hum);
	 Serial.print(",");
	 Serial.println(temp);
   }
}


/* Method to create the timestamp
 * Return: String with the date and time
 * Time format: YYYY-MM-DD HH:MM:SS
 *
 */
String timestamp()
{
  DateTime now = RTC.now();
 
  String year = String(now.year(), DEC);
  String month = String(now.month(),DEC);
  String day = String(now.day(), DEC);
  String hour = String(now.hour(), DEC);
  String minute = String(now.minute(), DEC);
  String second = String(now.second(), DEC);
  
  // The following if-statements corrects the format to always have two digits
  if(month.length() < 2)
	month = "0" + month;
	  
  if(day.length() < 2)
	day = "0" + day;
	
  if(hour.length() < 2)
	hour = "0" + hour;
	
  if(minute.length() < 2)
	minute = "0" + minute;
  
  if(second.length() < 2)
	second = "0" + second;
  
  String date = year + "-" + month + "-" + day;
  String time = hour + ":" + minute + ":" + second;
  
  return (date + " " + time);
}